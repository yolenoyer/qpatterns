#!/usr/bin/python

import qpatterns as qp


# GET MATCHES FROM GLOBAL VARIABLES:

def get_result():
    global QPATTERN, INPUT_STRING
    return qp.searchstr(INPUT_STRING, QPATTERN)


# DISPLAY FUNCTIONS:

def align(s, width):
    return len(s) < width and s + ' '*(width - len(s)) or s

def print_result(template_message, result):
    global QPATTERN, MATCH_JUSTIFY
    place_holder = align(result, MATCH_JUSTIFY) + "<== qpattern:  {}".format(QPATTERN)
    print(template_message.format(place_holder))

def print_matches(template_message):
    print_result('   - {}'.format(template_message), ', '.join(get_result()))

def SET_CONTEXT(input_string, match_justify):
    global INPUT_STRING, MATCH_JUSTIFY
    INPUT_STRING = input_string
    MATCH_JUSTIFY = match_justify

    print("In the following input string:")
    print("    {}".format(INPUT_STRING))
    print()


# MAIN SCRIPT:

SET_CONTEXT(
    input_string = "one two three four",
    match_justify = 8
)

QPATTERN = 'w'
print_matches('The first word is:     {}')
# ==> one

QPATTERN = '2w'
print_matches('The second word is:    {}')
# ==> two

QPATTERN = '-1w'
print_matches('The last word is:      {}')
# ==> four


print()


SET_CONTEXT(
    input_string = '(salut monde)  (one "))(" two three)  (people are strange)',
    match_justify = 25
)

QPATTERN = '2('
print_matches("The second parenthesis block contains:         {}")
# ==> one "))(" two three

QPATTERN = '"'
print_matches("The first string content I can find is:        {}")
# ==> ))(

QPATTERN = '*(-1w'
print_matches("The last word of each parenthesis block is:    {}")
# ==> monde, three, strange

