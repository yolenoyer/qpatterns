
import re

from .util import *


# Used to check the type of a regex object:
re_type = type(re.compile(''))




##
# Walk threw a string from start to end.
class BasicStr:

    ##
    # Return the remaining part of the string, depending of the current index.
    def remain_str(self):
        return self.string[self.index:]


    ##
    # Look for `to_look` at the current index in the string.
    #
    # @param to_look  String or regex object to search. If it's a list, check if at least one
    #                 element of the list matches.
    #
    # @return
    #    If `to_look` is a string or a regex object, then return True if it was found, False otherwise.
    #    If `to_look` is a list, then return the index of the found element in the list, or -1 if nothing
    #    was found.
    def look_for(self, to_look):

        if to_look is None: return False

        # If it's a list, at least one element must match:
        if isinstance(to_look, list):
            for i, each_to_look in enumerate(to_look):
                if self.look_for(each_to_look):
                    return i
            return -1

        # If `to_look` is a regex object:
        if isinstance(to_look, re_type):
            return self.look_for_regex(to_look)

        equal = self.remain_str()[:len(to_look)] == to_look
        if equal:
            self.last_look = to_look
            printdbg(4, 'look_for({}) : found'.format(to_look))
        return equal


    ##
    # Check if a regular expression matches at the current position in the string.
    #
    # `self.last_match` will contain the result (`re.MatchObject`) of the last search performed with
    # this method.
    #
    # @param regex  Regex to search for
    #
    # @return  True if found, False otherwise
    def look_for_regex(self, regex):
        self.last_match = regex.match(self.string, self.index)
        if self.last_match:
            # Put the found pattern in `self.last_look`:
            self.last_look = self.last_match.group(0)
            return True
        return False


    ##
    # Put the current index after the (eventually) found given search.
    #
    # @param search  Text to search (string or regex)
    def skip_search(self, search):
        found = self.look_for(search)
        if found:
            self.skip_last_look()
        return found


    ##
    # Skip one or more char(s) in the string.
    #
    # @return  `True` if we didn't reach the end of the string
    def go_forward(self, count = 1):
        self.index += count
        if self.index >= len(self.string):
            self.index = len(self.string)
            return False
        return True


    ##
    # Skip the last search performed with `look_for()`.
    def skip_last_look(self):
        if self.last_look is not None:
            self.go_forward(len(self.last_look))
            self.last_look = None


    ##
    # Return `True` if the index has reached the end of string.
    #
    # @return  Boolean
    def end(self):
        return self.index >= len(self.string)


    ##
    # Reset the index position to 0.
    def rewind(self):
        self.index = 0


    ##
    # Constructor
    #
    # @param string  String to handle
    def __init__(self, string):
        self.string = string
        self.index = 0
        self.last_look = None
        self.last_match = None

