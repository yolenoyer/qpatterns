
from .basicstr import *
from .block import *
from . import generic_syn



##
# Handle the read of a string, by taking into account block of parenthesis, square brackets,
# strings, by saving a stack of the current imbrication state.
# The recognized blocks are defined in generic_syn.py, or can be overloaded.
class SmartStr(BasicStr):

    #--------------------     def debug_disp_state(self)
    def debug_disp_state(self):
        output = 'index: {} , char: {}'.format(self.index, self.string[self.index])
        if self.in_string:
            output += ', String'
        output += '\n'

        output += "Blocks : {}".format(self.block_stack)

        print(output)


    ##
    # Add a new block imbrication level.
    #
    # @param block  A Block instance
    def block_stack_push_block(self, block):
        self.block_stack.append(block)

        self.in_string = block.block_def and block.block_def.is_string or False

        #  print('Push block: {}'.format(block.block_def.start_char))
        #  self.debug_disp_state()


    ##
    # Add a new block imbrication level, without knowing the position of the end of block for the
    # moment.
    #
    # @param block_def  Block definition (BlockDef)
    def block_stack_push(self, block_def):
        self.block_stack_push_block(Block(block_def, self.index, None))


    ##
    # Remove the last block imbrication level, only if the given block type matches.
    #
    # @param block_def  Block definition (BlockDef)
    def block_stack_pop(self, block_def):
        # If the stack is empty:
        if not self.block_stack: return

        last_block = self.block_stack[-1]

        # If the block types don't match, i.e.: "( ... ]":
        if last_block.block_def != block_def: return

        # If we reach the end of the "restriction" block, set the `out_of_scope` flag, which will
        # lead to finish the search:
        if last_block == self.restrict_block:
            printdbg(3, 'Out of scope: {}'.format(self.index))
            self.out_of_scope = True

        del self.block_stack[-1]

        # Remove the `in_string` flag if we just have removed a string block:
        if block_def and block_def.is_string:
            self.in_string = False

        #  printdbg(3, 'Pop block:')
        #  self.debug_disp_state()


    # Check if the current index is just on the start of a block.
    # Is so, append the block to the stack.
    def check_for_blocks(self):

        # If we are inside a string:
        if self.in_string:
            # Char escaping:
            if not self.string_escape and self.look_for('\\'):
                self.string_escape = True
                return
            if self.string_escape:
                self.string_escape = False
                return

            # Look for the end of string character:
            bd = self.block_stack[-1].block_def
            if bd and self.look_for(bd.end_char):
                self.block_stack_pop(bd)

            # Quit, in order to not test other kinds of blocks:
            return

        # Test each kind of block:
        for bd in self.bound_block_defs:
            if self.look_for(bd.start_char):
                self.block_stack_push(bd)
                break
            if self.look_for(bd.end_char):
                self.block_stack_pop(bd)
                break


    ##
    # Return the content of the string which starts at the current index.
    # On success, `index` will point after the found string.
    #
    # @return  The content of the found string (without delimitor)
    def get_inner_string(self):
        last_level = len(self.block_stack)

        # Make sure to start on good bases:
        self.last_look = None

        # It is implicit that the 1st char is the string delimitor;
        # `block_stack` will then gain a new element (string block):
        self.check_for_blocks()
        self.skip_last_look()

        # Now, `last_level` = current level - 1

        start = self.index
        return_string = None

        while not return_string and self.index < len(self.string):

            # Restriction by position:
            if self.restrict_block and self.restrict_block.end is not None:
                if self.index >= self.restrict_block.end:
                    break

            self.check_for_blocks()

            if len(self.block_stack) == last_level:
                return_string = self.string[start : self.index]

            if self.last_look:
                self.skip_last_look()
            else:
                self.go_forward()

        return return_string


    ##
    # Look for a string by forwarding the index, skipping inner string content, and
    # updating the block stack.
    #
    # @param search  String to search
    #
    # @return  True is the string was found
    def search_for(self, search, must_have_same_level = False):

        not_found_val = isinstance(search, list) and -1 or False

        begin_level = len(self.block_stack)
        while self.index < len(self.string):

            # Restriction by position:
            if self.restrict_block and self.restrict_block.end is not None:
                if self.index >= self.restrict_block.end:
                    break

            was_string = self.in_string
            had_level = len(self.block_stack)

            self.check_for_blocks()

            # If we reach the end of the restriction block:
            if self.out_of_scope: return not_found_val

            #  self.debug_disp_state()

            if (not must_have_same_level or begin_level == had_level) and \
                not (was_string and self.in_string):
                    found = self.look_for(search)
                    if found != not_found_val: return found

            self.go_forward()
        return not_found_val


    ##
    # Same as `search_for()`, but put the index after the search.
    #
    # @param search  String to search
    #
    # @return  True is the string was found
    def search_after(self, search, must_have_same_level = False):

        not_found_val = isinstance(search, list) and -1 or False

        found = self.search_for(search, must_have_same_level)
        if found != not_found_val:
            self.skip_last_look()
        return found


    ##
    # Return the input text comprised between the current index and the end of the restriction block
    # (or the end of string if there is no restriction block).
    #
    # @return  A tupple containing the text, its start position and its end position
    def get_text(self):
        # Skip starting spaces:
        while self.index < len(self.string) and self.string[self.index] in ' \t\r\n':
            self.index += 1

        start = self.index

        # If there is no restriction block, go the the end of string:
        if not self.restrict_block:
            text = self.string[self.index:]
        # If an end offset is defined, we skip the rest of the string:
        elif self.restrict_block.end is not None:
            text = self.string[self.index:self.restrict_block.end]
        # Otherwise, we assume there is a restriction block:
        else:
            start = self.index
            self.search_for(self.restrict_block.block_def.end_char, must_have_same_level = True)
            text = self.string[start:self.index]

        text = text.strip()
        return text, start, start + len(text)


    ##
    # Define a restriction block for future searches: these ones will stop at the end of the given
    # block (present in the `block_stack` list).
    #
    # @param block  Block which will limit the searches
    def restrict_inside_block(self, block):
        self.restrict_block = block
        # Remove the `in_string` flag even if it is one; so we can continue to search inside
        # strings, once we entered into them:
        self.in_string = False


    ##
    # Same as `restrict_inside_block()`, but use the last block where we entered.
    def restrict_inside_last_block(self):
        if self.block_stack:
            self.restrict_inside_block(self.block_stack[-1])


    ##
    # Make a deep clone of this SmartStr instance.
    #
    # @return  SmartStr
    def clone(self):
        c = SmartStr(self.string, self.bound_block_defs)
        c.index = self.index
        c.last_look = self.last_look
        c.last_match = self.last_match

        for bs in self.block_stack:
            c.block_stack.append(Block(bs.block_def, bs.start, bs.end))

        c.in_string = self.in_string
        c.string_escape = self.string_escape
        if self.restrict_block:
            rb = self.restrict_block
            c.restrict_block = Block(rb.block_def, rb.start, rb.end)
        c.out_of_scope = self.out_of_scope

        return c


    ##
    # Constructor
    #
    # @param string             String to handle
    # @param _bound_block_defs  If defined, replace the generic block definitions
    def __init__(self, string, _bound_block_defs = None):
        BasicStr.__init__(self, string)

        # Stack of the current blocks (list of Block objects):
        self.block_stack = []

        # Are we currently inside a string?
        self.in_string = False

        # Next character has to be escaped?
        self.string_escape = False

        # Restrict the search inner the given block:
        self.restrict_block = None

        # Is the index currently out of the search limits?
        self.out_of_scope = False

        # Block definitions to use:
        self.bound_block_defs = _bound_block_defs or generic_syn.bound_block_defs

