
import re

from .smartstr import *
from .command_classes import *



##
# Definition of the different recognized "bound" blocks:
bound_block_defs = [
    BlockDef('(', ')'),
    BlockDef('[', ']'),
    BlockDef('{', '}'),
    BlockDef('<', '>'),
    BlockDef('"', '"', is_string = True),
    BlockDef("'", "'", is_string = True),
    BlockDef("`", "`", is_string = True),
]




# Define the list of signs used by "bound" blocks, from `bound_block_defs`:
__bound_signs = {}
for bd in bound_block_defs:
    __bound_signs[bd.start_char] = bd
    __bound_signs[bd.end_char] = bd




##
# List of search block commands:
block_commands = [
    BlockCommandDef('(', ')', [ __bound_signs['('] ]),
    BlockCommandDef('[', ']', [ __bound_signs['['] ]),
    BlockCommandDef('{', '}', [ __bound_signs['{'] ]),
    BlockCommandDef('<', '>', [ __bound_signs['<'] ]),
    BlockCommandDef('\\"', '\\"', [ __bound_signs['"'], __bound_signs["'"], __bound_signs["`"] ]),
    BlockCommandDef("\\'", "\\'", [ __bound_signs['"'], __bound_signs["'"], __bound_signs["`"] ]),
    BlockCommandDef("'", "'", [ __bound_signs["'"] ]),
    BlockCommandDef('"', '"', [ __bound_signs['"'] ]),
    BlockCommandDef("`", "`", [ __bound_signs["`"] ]),
]




##
# List of delimitor commands:
delim_commands = []

# Names of delimitor commands which name is identical to the searched text:
__eponymous_delim_commands = [
    '==', '&&', '||', '->', '=>', '::',
    ',', ';', ':', '.', '+', '-', '=', '&', '|', '#', '%', '^', '_', '!', '~', '>'
]

for com in __eponymous_delim_commands:
    delim_commands.append(DelimCommandDef(com, [ BlockDef(com, None) ]))

# Delimitor commands to be escaped:
for delim in [ '/', '\\', '>', '<', '*', '-', '@', '$' ]:
    delim_commands.append(DelimCommandDef('\\'+delim, [ BlockDef(delim, None) ]))

delim_commands.extend([
    DelimCommandDef('\\&', [ BlockDef(oper, None) for oper in ['&&', '||'] ]),
])




# Define the signs used for the 'lhs' and 'rhs' commands for operator commands:
lhs_oper_sign = '@'
rhs_oper_sign = '$'




# List of the operator commands, sorted by string length:
__operators = [
    '===', '!==',
    '==', '!=', '>=', '<=', '~=', '+=', '-=', '*=', '/=', '&&', '||', '->', '=>', '::',
    '=', '+', '-', '*', '/', '&', '|', ':', '<', '>', '%', '.', '#'
]

##
# List of operator commands:
oper_commands = [ OperCommandDef(oper, [ BlockDef(oper, None) ]) for oper in __operators ]

oper_commands.extend([
    OperCommandDef('\\=', [ BlockDef(oper, None) for oper in [ re.compile('=[^=]'), '+=', '-=', '*=', '/=', '.=', '%=', ':=', '&=', '|=' ] ])
])




# Prepare the common part for the regex shortcut commands 'f' and 'F' (we add some impossible
# function names, in a generic programming language):
__f_shortcut_forbidden_funcnames = [ 'if', 'for', 'while', 'foreach', 'switch', 'not', 'and', 'or' ]
__f_shortcut_prepare = ''.join([ r'(?!{}\b)'.format(name) for name in __f_shortcut_forbidden_funcnames ])

##
# List of the regex shortcut commands:
regex_shortcut_commands = [
    RegexShortcutDef('w', r'\w+'),
    RegexShortcutDef('W', r'\S+'),
    RegexShortcutDef('n', r'[+-]?([0-9]*\.[0-9]+|[0-9]+\.[0-9]*|[0-9]+)'),
    RegexShortcutDef('i', r'[0-9]+'),
    # Function names:
    RegexShortcutDef('f', r'\b{}[a-zA-Z_][a-zA-Z0-9_]*(?=\s*\()'.format(__f_shortcut_prepare)),
    # Function names, including non-word characters:
    RegexShortcutDef('F', r'\b{}\S*\b(?=\s*\()'.format(__f_shortcut_prepare)),
]


