qpatterns v0.01
===============

A draft python library which helps to easily retrieve semantic informations inside string contents.

For example it can retrieve the inner content of a parenthesis block:

* while taking into account nested blocks
* smartly skipping the content of quoted strings

But it can retrieve far more complex nested information.

Qpatterns are designed in order to be very short; for example, in order to find the first
parenthesis block content of some input, the *qpattern* would just be the below single
character:

    (

You can search for more complexly-nested data; for example, here is how to find the 2nd
comma-separated part of the 3rd string:

	3'2,

You can also search for multiple results; for example, let's see how you can find all the numbers
in the input:

	*n


INSTALLATION
============

Clone the Github repository, then run the following command inside the project root:

    # pip install .

You can also install the tiny bash wrapper `qpat` by copying the python script `examples/qpat` in a
`$PATH` directory, for example:

    # cp examples/qpat /usr/local/bin


QUICK START
===========

After having installed the library like described above, write a Python script with the
following code:

    import qpatterns as qp

    input_str = 'one two three'

    # Get all the words in the input string:
    print(qp.searchstr(input_str, '*w'))

    # Get the last word in the input string:
    print(qp.searchstr(input_str, '-1w'))


WHAT IS `qpatterns`?
====================

`qpatterns` is a draft for a new kind of regular expression, which is focused on solving some
day-to-day issues, when needing to extract an information from a human-readable string.

The basic concept of `qpatterns` is guided by the recognition that *many* syntax patterns are
universally admitted: i.e., an opening bracket character like `(` will hugely often open a semantic
block which will naturally be closed by a `)` character. Similarly, commas and semicolons will
hugely often separate similar elements each other.

With this idea in mind, the goal was to find the quickest and easiest way to express the following
kinds of questions, considering a given input string:

* What is the content of the first bracket block ?
* What is the last word ?
* What is the second number found in each of the following square-bracket blocks ?
* etc...

**Pros**: the `qpatterns` library:

* is working well;
* is incredibly useful, at least for the author... Indeed, I wrote a Vim plugin companion, and now I
  really can't imagine working without these tools, because they solve unique problems in a short
  time, like reformatting an array, getting all string constants in a range of lines, getting all the
  arguments of a function into a list...
* can solve many questions I read on StackOverflow about pattern matching, especially when blocks of
  brackets have to be parsed in a reliable way, or when quoted strings cause issues with classical
  regex parsing;

**Cons**: the `qpatterns` library:
* is *extremely* slow; indeed is was written as a draft, and I can honnestly imagine that if it was
  written in a language like C/C++ (at least for critical parts), if a "pattern compile" feature
  was implemented, and if it was better written in general, it could easily run... hmmm... let's say,
  100x or 500x faster... Actually, the goal of this library is only to present the kind of pattern
  syntax which could then be written in a better way/language.
* is lacking major features:
  * multi-line support (not so hard to implement)
  * configurable definition of "what is a string exactly" without changing the code (this could be
    implemented *quite* easily actually)
  * a *custom delimiter* command, which would allow any string to behave like a delimiter (not so
	hard to implement)
  * conditionnal block searching (i.e.: find a bracket block WHERE there are at least 2
	comma-separated elements)
  * a dedicated *qpattern tag* syntax element, delimited by distinct (and simple... that is the
	problem because it will interfer with already existing syntax) opening and closing tokens.
	This *qpattern tag* could be used for anything else that can't be expressed simply, i.e. the
	conditionnal block searching feature, long names for custom regex commands, options, customized
	counts...

## Qpatterns vs. classical regular expressions

`qpatterns` is *not* a replacement for classical regex at all: it is rather designed as a
higher-level tool.

However, here are some comparisons around them:

* Regular expressions match characters... `qpatterns` matches generic semantic blocks of text.
* `qpatterns` includes regex as a native command; so it is designed as a higher-level tool.

### qpatterns vs. regex: Working with strings

With classical regex, it is particularly difficult to treat with strings.

For example, let's suppose you want to match the content of a parenthesis block with the help of a
regex pattern. Naively, you would try something like `/([^)]*)/`. But problems begin when strings
can be found inside the parenthesis block; indeed if the input is `(x, y, ")")`, then such a regex
will fail, because it will unfortunately stop on the inner-string `)` character.

`qpatterns` was initially created to handle this kind of problem; it natively understands what
"generic" strings look like, and skip smartly their content.

So, for the above input, the qpattern `(` will result exactly in what you would expect for, i.e.
`x, y, ")"` (without the outer parentheses).

### qpatterns vs. regex: Working with nested bracket blocks

With classical regex, a problem similar to the string problem (described just above) can quickly
happen, with nested bracket blocks. Indeed, trying to match a reliable bracket block which allows
nested brackets, can become a hard work while using regular expressions.

For example, let's suppose you want to match the content of a parenthesis block with a regex
pattern.  Naively, you would try something like `/([^)]*)/`. But what will happen with the following
input? : `(func(x, y, getColor()))` Scrap, only scrap.

`qpatterns` was initially created to handle this kind of problem, as well as the "string problem";
it natively parses nested blocks.

So for the above input, the qpattern `(` will result exactly in what you would expect for, i.e.
`func(x, y, getColor())`, (without the outer parentheses).


API OVERVIEW
============

* ### `qpatterns.search(text, qpattern)`

  Perform a search: return the list of matches.  
  The matches are objects which have the following properties:
  * `match`: the matched string
  * `start`: start position of the match inside the source string
  * `end`: end position of the match (exclusive) inside the source string
  * `source`: the source string (i.e. the above `text` argument)

  If no result was found, return an empty list.

  *Note*: If no *star count* was used in the qpattern, the returned list will never have more than
  one element.

* ### `qpatterns.searchstr(text, qpattern)`

  The same as `qpattern.search()`, but simply return the list of matched strings.

* ### `qpatterns.searchdict(text, qpattern)`

  The same as `qpattern.search()`, but return dictionnaries instead of `QMatch` objects, with keys
  having the same name as the `QMatch` object properties (`'match'`, `'start'`, `'end'`, `'source'`).


PATTERN SYNTAX
==============

The syntax for qpatterns has been designed with one priority in mind: to keep it as simple as
possible, trying to retain the most obvious solution for each feature. This goal has not been
totally reached, and that's why the below syntax is only an ***early draft***, which will likely be
deeply modified in future versions.

## Reserved characters

Actually, the punctuation signs which are reserved for the `qpattern` syntax are the following:

**`<` `>` `@` `$` `/` `*` `\` `-`**

All other punctuation signs express themselves, either as a block (like `(` or `[`) or as a
delimiter (like `,` or `;`).

See below the part "Some nuances about the reserved characters" for more details about reserved
characters.

## General definition of the syntax

A qpattern is a series of *commands*, optionnally separated by spaces.  It has the following general
form:

    [count]<command> [count]<command> [count]<command>  ...

The last command is very important, because it will become the result of the whole qpattern.

Each command will look forward into the input text to match for a particular pattern:
* Some commands (called ***entering*** commands) will restrict the search to a particular zone of
  the input (for example, the `(` command will restrict further commands to search into the content
  of the first found parenthesis `()` block).
* Some other commands (called ***non-entering*** commands) will just look for a simple token,
  allowing further commands to continue to parse the rest of the input (for example, the `w` command
  will search for the next word, but obviously will not restrict further commands to search *into*
  this word: the whole rest of the input, after the found word, will be available for search).

For each kind of command (*entering* or *non-entering*), it is possible to force the opposite
*entering* behaviour:
* It is possible to explicitly enter into a *non-entering* command, by using `>`. It is explained
  below with more details (see the part **Regex command**).  
* It is possible to explicitly **not** enter into an *entering* command: for a *block* command, you
  just need to close the block immediately (i.e. `(` becomes `()`); for a *delimiter* command, you
  have to use the suffix `<` just after the command, without space; all of this is explained below
  with more details, in the corresponding command categories (see **Block commands**, and
  **Delimiter commands**).

Spaces between two commands are optionnal.  However in some cases, spaces are required to
distinguish commands each other. For example, the qpatterns `!=` and `! =` will perform different
searches.

## Counts

Most commands have an associated count. It always means the same: if `n` is the count, then it
means:

> Find the `n`'s occurence of this *block* / *delimited block* / *regex*

When the count is not given for a command, it always defaults to `1`.

A count can be negative; in this case, it means quite the same as with Python's arrays:

> Find the `n`'s **last** occurence of this *block* / *delimited block* / *regex*

### Star counts (`*`)

The *star count* feature is maybe the most powerful feature in `qpatterns`:

Instead of being a number, a count can be a star character `*`. If so, multiple results will be
enabled.

For example, the qpattern `*w` will result in all the found words in the input text.

A *star count* can be placed anywhere in the qpattern flow.

For example, with the input `(hello world) (big ben) (brother hood)`, the qpattern `*(w` would
result in the first word of each parenthesis block, i.e. the three following elements: `hello`,
`big`, `brother`.

### Multiple star counts

*Star counts* can be used with several commands in the same qpattern. If so, all the possible
combinations will be recursively treated.

For example, with the input `(number 6, number 8), 9, 12, (number 3)`, the qpattern `*(*n` will
return *all* the numbers found in *all* the parenthesis blocks, i.e. `6`, `8` and `3`.

*Note*: Theorically, two *star counts* would result in a 2D array; three *star counts* would result
in a 3D array, and so on. But for now, results are given as a flattened 1D array.

## Command categories

Commands can be classified into several categories:

* ### Block commands

  Block commands, as explained above, will search for a particular input zone (a *block*), and if
  found, will restrict further pattern commands to search only inside this zone. The blocks to
  search in the input are typically bounded by separators like `(` and `)`, `[` and `]`...

  If a count `n` is given, then the `n`'th block will be searched in the same nesting level. It
  implies that the input is parsed to check if other similar blocks are nested, in order to skip
  them.

  For example, for the following input: `(A) (B(C)) (D)`, the qpattern `2(` will return `B(C)`, and
  the qpattern`3(` will return `D`.
  In order to go deeper in nesting, you have to repeat the block command. For example, `2((`
  will return `C`.

  Four block commands are available:

  **`(` `[` `{` `<`**

  *Note*: `<` blocks have to be used with care; indeed, the `<` char can actually be used as a
  suffix with delimiter commands (to make them `non-entering` commands). So if a `<` block command
  is just following a delimiter command, make sure to add a space between, in order to make the
  difference.

* ### *Non-entering* block commands

  Sometimes when searching, you don't need to *enter* into blocks, but rather *skip* them to go
  further into the same nesting level.

  For example, with the input `(one) (two) IMPORTANT`, if you use the pattern `2(w`, you will not
  get the important word, because you will enter into the 2nd block, then the result will be `two`.
  In order to indicate that you *don't want* to enter into a given block, close the opening block
  command just after opening it: `2()w`. This will convert the block command to a *non-entering*
  block command.

  This can be done with every block command:

  **`()` `[]` `{}` `<>`**

  *Note*: you can can't put anything between the opening character and the closing character, even
  any space; this would not be interpreted as a *non-entering* block command, then would lead either
  to a misinterpreted pattern or a syntax error.

* ### String commands, and *non-entering* string commands

  String commands are very similar to block commands, with an exception: they are not nestable,
  at least for a particular opening sign.

  ***Important***: for now, `qpatterns` considers that *any* backslash character `\` inside a string
  is used to escape the next character, which is not reliable, depending on the used language.  For
  example, the following Bash string would *not* be correctly interpreted: `s='ending
  backslash...\'`  
  Being able to setup how strings have to be parsed is one of the biggest priority in future
  development.

  You can transform string commands into *non-entering* string commands by doubling their sign, i.e.
  `"` would become `""`.

  There are three string commands, and three corresponding *non-entering* string commands:

  **`'` `"` `` ` ``**  
  **`''` `""` ``` `` ```**

  There are also two other string commands, which will both search for any kind of string
  (either simple-quoted, double-quoted or backtick-quoted) without distinction (and their awful
  *non-entering* doubled commands):

  **`\'` `\"`**  
  **`\'\'` `\"\"`**

* ### Delimiter commands

  In many cases, several relevant input datas are separated by delimiters. For example in many
  programming languages, function arguments are separated by commas. You can use qpatterns to
  quickly access them.

  Using a delimiter command basically means that the current input (eventually restricted by earlier
  commands) has to be splitted by the given token. Then the count of the command is used to choose
  the desired element.

  For example, with the input `12, 36 + 98, 45`, the qpattern `2,` will result in `36 + 98`.

  `qpatterns` is a text parser at first. So, in the case of function arguments, the input will have
  first to be restricted by a `(` command. Indeed, for example with the input `add(a, b);`, the
  qpatterns `1,` and `2,` will result respectively in `add(a` and `b);`, because it will split the
  whole input data, without taking parentheses into account.  In order to get the arguments, you
  first have to restrict the input with the `(` command; the corrected qpatterns `(1,` and `(2,`
  will result respectively in `a` and `b`.

  There are many delimiter commands:

    **`,` `;` `:` `.` `+` `-` `=` `&` `|` `#` `%` `^` `_` `!` `~` `==` `&&` `||` `->` `=>` `::`**

  Some of them have to be escaped with `\`:

    **`\/` `\\` `\>` `\<` `\@` `\$` `\*` `\-`**

  *Note* that the minus `-` delimiter can be used in its escaped form as well as its non-escaped
  form.  But the non-escaped form can only be used with an explicit count, otherwise the minus sign
  would be interpreted as the beginning of a negative count.

* ### *Non-entering* delimiter commands

  Delimiter commands are *entering* commands. It means that further commands will be restricted to
  the content inside the delimiters you choose.

  To indicate that you *don't want* to enter into the block, add the `<` suffix just after the
  command. For example, `2,` would become `2,<`. Further commands will then work on the rest of the
  input data, *after* the given delimited block.

* ### Operator commands

  *Operator* commands are used to enter into the left-hand side (*lhs*) or the right-hand side
  (*rhs*) of an operation:

  * For *lhs*, it is formed by the `@` character followed by the desired operator
  * For *rhs*, it is formed by the `$` character followed by the desired operator

  For example, with the input `a = 2 + 3`, the qpattern `@=` will result in `a`, and the qpattern
  `$=` will result in `2 + 3`.

  Counts are forbidden with *operator* commands, simply because they wouldn't mean anything.

  *Note*: when an *operator* command is also available as a *delimiter* command (i.e., `@=` and `$=`
  *operator* commands are available, while `=` *delimiter* command is available as well), then its
  behaviour can be reproduced with *delimiter* commands:
  * For *lhs* commands, the qpattern `@=` can be reproduced with `1=` or simply `=`
  * For *rhs* commands, the qpattern `$=` can be reproduced with `=<$`

  However, *operator* commands are more numerous than the *delimiter* commands, because they don't
  interfer with other commands thanks to the `@` and `$` signs, which are hugely unused within
  generic-language operators.

  Each of the following operators are valid operator commands:

  **`@=` `@+` `@-` `@*` `@/` `@&` `@|` `@:` `@<` `@>` `@%` `@.` `@#`**  
  **`@=` `@!=` `@>=` `@<=` `@~=` `@+=` `@-=` `@*=` `@/=` `@&&` `@||` `@->` `@=>` `@::`**  
  **`@===` `@!==`**

  **`$=` `$+` `$-` `$*` `$/` `$&` `$|` `$:` `$<` `$>` `$%` `$.` `$#`**  
  **`$=` `$!=` `$>=` `$<=` `$~=` `$+=` `$-=` `$*=` `$/=` `$&&` `$||` `$->` `$=>` `$::`**  
  **`$===` `$!==`**

  The special operator commands `@\=` and `$\=` take into account any assignement operator
  containing the equal sign `=` (`=`, `*=`, `+=`...)

* ### Regex command

  The regex command has the following form:

      [count]/<regex>/

  It searches for the `n`'th occurence of the given Perl-style `<regex>` in the input. For the
  moment, there is no available flag to activate a case-insensitive search. Sorry.

  The regex command is a *non-entering* command. If you want to force entering it (restrict further
  commands to the matched zone), append `>` after then regex, i.e.: `/<regex>/>`.

* ### Regex shortcut commands

  Some alphabetic letters are used as regex shortcut commands. One of them is `w` (often used in
  this help file), which searches for word identifiers.

  Here are the actual existing shortcuts, with their associated regex:

    * `w` Search for words (`/\w+/`)
    * `W` Search for blocks of non-space characters (`/\S+/`)
    * `n` Search for real numbers (`/[+-]?([0-9]*\.[0-9]+|[0-9]+\.[0-9]*|[0-9]+)/`)
    * `i` Search for integer numbers (`/[0-9]+/`)
    * `f` Search for function names (identifiers followed by `(`, skipping generic keywords like
      `if`, `for`...)
	* `F` Search for extended function names (like `f`, but allows non-word characters, as with
	  `MyClass::method`)

* ### Rest-of-the-line command (`$`)

  When a qpattern matches successfully, it uses its last command as the result. But sometimes you
  want to match the rest of the input, i.e. what is remaining *after* the last command. For example,
  with the input `(annoying things) Important things`, if you try the qpattern `(` or even `()`,
  then you will get annoying things.

  In order to get the rest of the line, you can use the `$` at the whole end of the pattern. It has
  to be used in conjunction with a *non-entering* command. Indeed, using an *entering* command like
  in the qpattern `($` will first enter the `(` block, then result in... the rest of the already
  restricted block, in occurence `annoying things`.

  In this example, the right way to use it is `()$`, which will result in `Important things`.

  Another example: in the following input:

      2018-09-01 13:14:50  Log message

  If you want to get only the log message, you can get the 2nd WORD (`2W`), then use `$` to get the
  rest of the line: the right qpattern would be `2W$`.

## Some nuances about the reserved characters

As explained above, reserved characters are the ones listed below:

**`<` `>` `@` `$` `/` `*` `\` `-`**

Here are some nuances about there "reserved" nature:

* all these characters have their escaped *delimiter* command version; i.e., to look for a backslash
  `\` delimiter, use `[count]\\`.
* the `<` character is only "reserved" when directly following a *delimiter* command, without
  spacing.  In this case, use a space to distinguish. In all other situations, it can be used
  normally, i.e.  as a block command (for XML-like tags for example), or as a delimiter with its
  escaped version `\<`
* the `>` character is only "reserved" when directly following a *non-entering* command, like `W`, or
  like the regex command. But it can be used normally to close `<` blocks, like this: `<>`; and it
  can be used normally as well as a delimiter, with `>` (when not in the scope of this explanation)
  or with its escaped version `\>` (this last one works in any case).
* the minus sign `-` is only reserved because it can be the start of a negative count. But if a
  count is explicitly given before, then the minus sign will always be interpreted as a standard
  *delimiter* command.  In other words, the minus sign, as a *delimiter* command, needs a mandatory,
  explicit count.


PATTERN EXAMPLES
================

* ## Example 1: basics  (part 1)

    This example demonstrates basic uses of qpatterns.

    **Input**:

        Hello world, here is a (very basic) input example

    **Results with different qpattern expressions**:

    Qpattern | Result                                 | Explanations
    ---------|----------------------------------------|------------------
    `1w`     | `Hello`                                | The 1st word (with explicit count)
    `2w`     | `world`                                | The 2nd word
    `w`      | `Hello`                                | The 1st word (the count defaults to `1`)
    `wwww`   | `is`                                   | The 4th word (can be more simply written as `4w`)
    `-1w`    | `example`                              | The last word (negative counts get elements from the end)
    `(`      | `very basic`                           | The 1st parenthesis block
    `(w`     | `very`                                 | The 1st word in the 1st parenthesis block
    `,`      | `Hello world`                          | The 1st comma-separated part (the count defaults to `1`)
    `2,`     | `here is a (very basic) input example` | The 2nd comma-separated part

* ## Example 2: basics (part 2)

    This example demonstrates basic uses of qpatterns.

    **Input**:

        arr = [ 23, 78, 97 ]  # This is a comment

    **Results with different qpattern expressions**:

    Qpattern | Result                   | Explanations
    ---------|--------------------------|-----------------------
    `w`      | `arr`                    | The 1st word (the count defaults to `1`)
    `[`      | `23, 78, 97`             | The 1st square bracket block
    `[*n`    | `23`<br>`78`<br>`97`     | All numbers in the 1st square bracket block
    `#`      | `arr = [ 23, 78, 97 ]`   | The 1st `#`-separated part (default count = `1`)
    `2#`     | `This is a comment`      | The 2nd `#`-separated part

* ## Example 3: including Perl regular expressions

    This example demonstrates how to include regular expressions in qpatterns.

    **Input**:

        Completed: 10%, Remaining: 90%, Free mem: 15%

    **Results with different qpattern expressions**:

    Qpattern          | Result                   | Explanations
    ------------------|--------------------------|-----------------------
    `/\d*%/`          | `10%`                    | The 1st string matching the given regex
    `/Remain/ /\d*%/` | `90%`                    | First, match `/Remain/`, then match the next percentage (final result)
    `-1/\d*%/`        | `15%`                    | The last string matching the given regex

* ## Example 4: lhs and rhs parts of an operator (part 1)

    This example shows how to use the `@...` and `$...` commands.

    **Input**:

        x = 2 * y

    **Results with different qpattern expressions**:

    Qpattern | Result  | Explanations
    ---------|---------|-------------
    `@=`     | `x`     | The left-hand side (lhs) of the `=` operator
    `$=`     | `2 * y` | The right-hand side (rhs) of the `=` operator

* ## Example 5: lhs and rhs parts of an operator (part 2)

    This example shows how to use the `@...` and `$...` commands in more details.

    **Input**:

        a = 5, b = 78 + 36, c = "hello"

    **Results with different qpattern expressions**:

    Qpattern | Result                        | Explanations
    ---------|-------------------------------|--------------------
    `@=`     | `a`                           | The lhs of the `=` operator
    `$=`     | `5, b = 78 + 36, c = "hello"` | The rhs of the `=` operator: no rule for splitting commas!
    `,`      | `a = 5`                       | The 1st comma-separated part
    `,@=`    | `a`                           | The lhs of the 1st comma-separated part
    `,$=`    | `5`                           | The rhs of the 1st comma-separated part
    `2,`     | `b = 78 + 36`                 | The 2nd comma-separated part
    `2,@=`   | `b`                           | The lhs of the 2nd comma-separated part
    `2,$=`   | `78 + 36`                     | The rhs of the 2nd comma-separated part
    `*,`     | `a = 5`<br>`b = 78 + 36`<br>`c = "hello"` | All comma-separated parts
    `*,@=`   | `a`<br>`b`<br>`c`             | The lhs of each comma-separated part
    `*,$=`   | `5`<br>`78 + 36`<br>`"hello"` | The rhs of each comma-separated part
    `*n`     | `5`<br>`78`<br>`36`           | All numbers without distinction

* ## Example 6: strings (part 1)

    This example shows how to match strings.

    **Input**:

        s1 = "double-1", s2 = 'single', s3 = "double-2"

    **Results with different qpattern expressions**:

    Qpattern | Result                               | Explanations
    ---------|--------------------------------------|--------------------
    `*'`     | `single`                             | All single-quoted strings
    `*"`     | `double-1`<br>`double-2`             | All double-quoted strings
    `*\'`    | `double-1`<br>`single`<br>`double-2` | All strings
    `*\"`    | `double-1`<br>`single`<br>`double-2` | Idem (all strings)

* ## Example 7: string content is not searched

    This example demonstrates that string contents are not taken into account, unless
    you explicitly specify a string command in order to "enter" into strings.

    **Input**:

        n1 = 78, n2 = 23, s = "A string with a number, 96"

    **Results with different qpattern expressions**:

    Qpattern | Result                             | Explanations
    ---------|------------------------------------|--------------------
    `*n`     | `78`<br>`23`                       | The number in the string is not taken into account because it is "hidden" in the string
    `-1,`    | `s = "A string with a number, 96"` | The comma in the string is not taken into account, for the same reason
    `"*n`    | `96`                               | All numbers in the 1st string
    `",`     | `A string with a number`           | The 1st comma-separated part in the 1st string

 * ## Example 8: forcing entering a block

    This example shows how you can enter into blocks which are naturally *non-entering* blocks, like words (`w`).

    **Input**:

        setColour getColour putProperty

    **Results with different qpattern expressions**:

    Qpattern   | Result                                      | Explanations
    -----------|---------------------------------------------|--------------------
    `*w`       | `setColour`<br>`getColour`<br>`putProperty` | All words
    `*w>/.../` | `set`<br>`get`<br>`put`                     | `>` forces to enter into tokens, like `w`

 * ## Example 9: avoiding entering a block

    This example shows how you can skip blocks which are naturally *entering* blocks, like parenthesis blocks `(`.

    **Input**:

        A sentence (with parenthesis) then a conclusion (one, two, three, four).

    **Results with different qpattern expressions**:

    Qpattern | Result                     | Explanations
    ---------|----------------------------|--------------------
    `(`      | `with parenthesis`         | Enter the 1st parenthesis block
    `(w`     | `with`                     | Enter the 1st parenthesis block, 1st word
    `()w`    | `then`                     | Skip the 1st parenthesis block, then 1st word of what follows
    `()$`    | `then a conclusion (one, two, three, four).`  | Skip the 1st parenthesis block, then the rest of the line
    `2(,`    | `one`                      | Enter the 2nd paren. block, 1st comma-separated part)
    `2(,w`   | `one`                      | Enter the 2nd paren. block, enter the 1st comma-separated part, 1st word
    `2(,<w`  | `two`                      | You can skip a delimiter-separated block by appending `<`
    `2(,<*w` | `two`<br>`three`<br>`four` | Idem (skip one comma-separated element), but get *all* the following words

 * ## Example 10: using the rest-of-the-line command `$` (part 1)

    **Input**:

        17:08:25 29/09/2018  My log message (from a certain user), ok.

    **Results with different qpattern expressions**:

    Qpattern | Result                                      | Explanations
    ---------|---------------------------------------------|--------------------
    `2W`     | `29/09/2018`                                | Get 2nd WORD
    `2W$`    | `My log message (from a certain user), ok.` | Get 2nd WORD, then keep only what follows
    `(`      | `from a certain user`                       | Get the 1st paren. block
    `(w$`    | `a certain user`                            | Enter into the 1st paren. block, get 1st word, then keep only what follows (`$` works also *inside* blocks like parentheses)
    `()$`    | `, ok.`                                     | Skip the parenthesis block and get what follows

 * ## Example 11: using the rest-of-the-line command `$` (part 2)

    **Input**:

        one; two; three; four; five.

    **Results with different qpattern expressions**:

    Qpattern | Result                                     | Explanations
    ---------|--------------------------------------------|--------------------
    `;<$`    | `two; three; four; five.`                  | Use `<` after a delimiter to skip it, then get what follows with `$`
    `2;<$`   | `three; four; five.`                       | Idem, but skip two `;`-separated parts

